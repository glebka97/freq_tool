#include "token_reader.h"

#include <istream>
#include <optional>
#include <string>

namespace {
    bool IsAlpha(char sym) {
        return ('a' <= sym && sym <= 'z') || ('A' <= sym && sym <= 'Z');
    }
}  // namespace

TokenReader::TokenReader(std::istream& input_stream) : input_stream_(input_stream) {
    ReadToken();
}

const std::string& TokenReader::GetToken() const {
    return token_.value();
}

void TokenReader::Next() {
    ReadToken();
}

bool TokenReader::IsValid() const {
    return token_.has_value();
}

void TokenReader::ReadToken() {
    std::string token;

    for (char sym = input_stream_.peek(); sym != EOF && !IsAlpha(sym);
            input_stream_.get(), sym = input_stream_.peek()) {
        continue;
    }

    for (char sym = input_stream_.peek(); sym != EOF && IsAlpha(sym);
            input_stream_.get(), sym = input_stream_.peek()) {
        token.push_back(sym);
    }

    if (token.empty()) {
        token_ = std::nullopt;
        return;
    }

    token_ = std::move(token);
}
