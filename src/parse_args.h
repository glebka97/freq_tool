#pragma once

#include <filesystem>

struct Args {
    std::filesystem::path input_file_path;
    std::filesystem::path output_file_path;
};

Args ParseArgs(int argc, const char** argv);
