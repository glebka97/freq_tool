#pragma once

#include <istream>
#include <optional>
#include <string>

class TokenReader {
public:
    TokenReader(std::istream& input_stream);

    const std::string& GetToken() const;

    void Next();

    bool IsValid() const;

private:
    void ReadToken();

private:
    std::istream& input_stream_;
    std::optional<std::string> token_;
};
