#include "parse_args.h"

#include <stdexcept>

Args ParseArgs(int argc, const char** argv) {
    if (argc != 3) {
        throw std::logic_error("Invalid arguments count.");
    }

    return {argv[1], argv[2]};
}
