#include "parse_args.h"
#include "token_reader.h"
#include "word_freq_calculator.h"

#include <fstream>

void CalculateWordFrequencies(std::istream& input_stream, std::ostream& output_stream) {
    WordFreqCalculator freq_calculator;

    TokenReader reader(input_stream);
    for (; reader.IsValid(); reader.Next()) {
        const auto& token = reader.GetToken();
        freq_calculator.AddToken(token);
    }

    WriteWordFrequencies(output_stream, freq_calculator);
}

int main(int argc, const char** argv) {

    auto args = ParseArgs(argc, argv);

    std::ifstream input_file_stream(args.input_file_path);
    std::ofstream output_file_stream(args.output_file_path);

    CalculateWordFrequencies(input_file_stream, output_file_stream);

    return 0;
}
