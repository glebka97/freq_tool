#pragma once

#include <ostream>
#include <string>
#include <unordered_map>
#include <vector>

struct WordStat {
    std::string word;
    int frequency;
};

class WordFreqCalculator {
public:
    WordFreqCalculator();

    void AddToken(const std::string& token);

    std::vector<WordStat> ExtractWordStats() const;

private:
    std::unordered_map<std::string, int> words_frequency_;
};

void WriteWordFrequencies(std::ostream& os, const WordFreqCalculator& freq_calculator);
