#include "word_freq_calculator.h"

#include <algorithm>
#include <cctype>
#include <iomanip>
#include <iterator>
#include <tuple>

namespace {
    std::string NormalizeToken(const std::string& token) {
        std::string normalized_token;
        normalized_token.reserve(token.size());

        auto normalize_symbol = [](char sym) {
            return std::tolower(sym);
        };
        std::transform(token.begin(), token.end(),
                std::back_inserter(normalized_token), normalize_symbol);

        return normalized_token;
    }
}  // namespace

WordFreqCalculator::WordFreqCalculator() {}

void WordFreqCalculator::AddToken(const std::string& token) {
    ++words_frequency_[NormalizeToken(token)];
}

std::vector<WordStat> WordFreqCalculator::ExtractWordStats() const {
    std::vector<WordStat> word_stats;
    word_stats.reserve(words_frequency_.size());

    auto convert = [](const auto& kv) {
        return WordStat{kv.first, kv.second};
    };
    std::transform(words_frequency_.begin(), words_frequency_.end(),
            std::back_inserter(word_stats), convert);

    return word_stats;
}

void WriteWordFrequencies(std::ostream& output_stream, const WordFreqCalculator& freq_calculator) {
    auto word_stats = freq_calculator.ExtractWordStats();

    auto word_stat_compare = [](const WordStat& lhs, const WordStat& rhs) {
        return std::tie(rhs.frequency, lhs.word) < std::tie(lhs.frequency, rhs.word);
    };
    std::sort(word_stats.begin(), word_stats.end(), word_stat_compare);

    constexpr int kFrequencyWidth = 5;
    for (const auto& [word, frequency] : word_stats) {
        output_stream << std::setw(kFrequencyWidth) << frequency << " " << word << '\n';
    }
}
